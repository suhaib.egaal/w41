import React, {useState} from "react";
import Interval from "./interval.jsx";

function App() {
  const [time, setTime] = useState([]);

  return (
    <div className="App" style={{textAlign: 'center'}}>
      <p>learn react</p>
      <p>w41</p>
      <Interval time={time} setTime={setTime} />
      {time.map((time, i) => (
        <div key={i}>{time}</div>
      ))}
    </div>
  );
}

export default App;
